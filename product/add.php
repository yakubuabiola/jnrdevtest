<?php
include "../autoload.php";
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="data:;base64,iVBORw0KGgo=">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="../libs/style.css">
</head>
<body>

	<nav class="navbar navbar-expand-lg sticky-top navbar-light bg-light">
		<a class="navbar-brand" href=""><b>Add Product Page</b></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item ">
					<a href="../"><button class="btn btn-outline-success my-2 my-sm-0" type="submit"> <i></i>Back to Product</button></a>
				</li>
			</ul>
		</div>
	</nav>
	<div class="main-content" >
		<div class="container "> 

			<div class="productcont space-wrapper">
				<div class="row" >

					<div class="row justify-content-center">
						<div class="col-md-10 form-width">
							<!-- form user info -->
							<div class="card card-outline-secondary">
								<div class="card-header">
									<h3 class="mb-0 text-upper" style="">Add Product</h3>
								</div>
								<div class="card-body">
									<form autocomplete="off" class="form" id="addProductFrom" role="form" name="myform" method="POST" onsubmit="return validateform(event)" >
										<div class="alert-alert" ><p id="addAlert"><p></div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label form-control-label">Sku</label>
											<div class="col-lg-9">
												<input class="form-control" type="text" value="" name="sku" placeholder="sku" required="" oninvalid="this.setCustomValidity('Sku cannot be empty.')" onchange="this.setCustomValidity('')">
												<div class=" form-text  error-display" id="skuErr"></div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label form-control-label"> Name</label>
											<div class="col-lg-9">
												<input class="form-control" type="text" value="" name="name" placeholder="Product Name"  required="" oninvalid="this.setCustomValidity('Product name cannot be empty.')" onchange="this.setCustomValidity('')">
												<div class=" form-text  error-display" id="nameErr"></div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label form-control-label">Price</label>
											<div class="col-lg-9 input-group  mb-2"> 
												<div class="input-group-prepend">
													<div class="input-group-text">$</div>
												</div>
												<input class="form-control" type="number"  step="0.01" min="0" name="price" value="" placeholder="00.00"  required="" oninvalid="this.setCustomValidity('Product price cannot be empty.')" onchange="this.setCustomValidity('')">
												<div class=" form-text  error-display" id="priceErr"></div>
											</div>
										</div>
										<div class="form-group row">
											<label class="col-lg-3 col-form-label form-control-label">Type Switcher</label>
											<div class="col-lg-9">
												<select class="form-control" id="switcher" name="type" onchange="setSwitcher(this);"  required="">
													<option selected="" disabled="">Type Switcher</option>
													<option value="DVD">DVD</option>
													<option value="Book">Book</option>
													<option value="Furniture">Furniture</option>
												</select>
												<div class=" form-text  error-display" id="switchErr"></div>
											</div>
										</div>

										<div id="popout" ></div>	
									

										<div class="form-group row">
											<label class="col-lg-3 col-form-label form-control-label"></label>
											<div class="col-lg-9">
												<button class="btn btn-secondary" onclick="cancelReturn();" > Cancel </button>
												<input class="btn btn-primary" type="submit" value="Save">
											</div>
										</div>
									</form>
								</div>
							</div><!-- /form user info -->
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="../libs/cart.min.js"></script>
</body>
</html>