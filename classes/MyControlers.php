<?php

namespace classes;
use myEnv\DotEnv;
global $base_url;
global $path;
new DotEnv( $path. '.env');
use PDO;
class MyControlers
{
	public $conn;
	protected $dbhost;
	protected $dbuname;
	protected $dbpwd;
	protected $dbname;

	public function __construct()
	{
		$this->dbhost = getenv('DATABASE_HOST');
		$this->dbuname = getenv('DATABASE_USER');
		$this->dbpwd = getenv('DATABASE_PASSWORD');
		$this->dbname = getenv('DATABASE_NAME');
		$this->conn();        						
    }

	public function conn()
	{
		try{			
            $this->conn = new PDO("mysql:host={$this->dbhost};dbname={$this->dbname};", $this->dbuname, $this->dbpwd);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);	
            $this->conn->exec('set names utf8');		
        }catch(Exception $e){
            throw new Exception($e->getMessage()); 
            die();  
        }

	}

	public function select()
	{
		try{
			$parameters = [];
			$stmt = $this->executStatements('SELECT * FROM products GROUP BY id desc',$parameters);
			return $stmt->fetchAll();
		}catch(Exception $e){
            throw new Exception($e->getMessage());   
        }
	}

	public function addProducts($statement,$parameters)
	{
		try {
			$stmt = $this->executStatements($statement,$parameters);
			return $this->conn->lastInsertId();
		} catch(PDOException $e) {
			throw new Exception($e->getMessage()); 
		}
	}

	public function removeProducts($list, $arrays)
	{
		try {
          	$stmt = $this->conn->prepare("DELETE FROM products WHERE id = :id AND productId = :productId");
          	for ($i = 0; $i <  $list; $i++) {
          	$stmt->execute(['id' => $arrays[$i]['id'],'productId' => $arrays[$i]['prodId']]);
          	}
          	return $stmt->rowCount();
		} catch (PDOException $e) {
			throw new Exception($e->getMessage()); 
		}
	}

	private function executStatements( $statement = "" , $parameters = [] )
	{
		try {
			$stmt = $this->conn->prepare($statement);
			$stmt->execute($parameters);
			return $stmt;
		} catch(PDOException $e) {
			throw new Exception($e->getMessage()); 
		}
	}
	
}