<?php
include "../autoload.php";
$newobj = new classes\MyControlers();

# fetch products
if(isset($_POST['action']) && $_POST['action'] === 'fetchProducts'){
	$res = $newobj->select();
	echo json_encode($res);
}

# add products
if(isset($_POST['action']) && $_POST['action'] === 'addProducts'){
	($_POST['switcher'] == 'DVD') ? $desc = 'Size':(($_POST['switcher'] == 'Book')?$desc = 'Weight':$desc = 'Dimensoin');
	$statement = "INSERT INTO products (productId,sku,productName,price,productType,description) VALUES (:productId, :sku, :productName, :price, :productType,:description)";
	$parameters = [
		'productId' => uniqid(),
		'sku' =>  $_POST['sku'],
		'productName' =>  $_POST['prodName'],
		'price' =>  $_POST['price'],
		'productType' =>  $_POST['switcher'],
		'description' =>  $desc . ' : '.$_POST['description']
		];
	$result = $newobj->addProducts($statement,$parameters);
	echo is_int(filter_var($result, FILTER_VALIDATE_INT)) ? json_encode('true'):die(json_encode('error'));
}

#delete products
if(isset($_POST['action']) && $_POST['action'] === 'deleteProducts'){
	$arrays = $_POST['postValue'];
	$list = count($arrays);
	$res = $newobj->removeProducts($list, $arrays);
	echo is_int(filter_var($res, FILTER_VALIDATE_INT)) ? json_encode('true'):die(json_encode('error'));
}



