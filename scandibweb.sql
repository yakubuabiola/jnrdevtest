-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 14, 2021 at 03:03 AM
-- Server version: 10.3.25-MariaDB-0ubuntu0.20.04.1
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandibweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `productId` varchar(100) NOT NULL,
  `sku` varchar(50) NOT NULL,
  `productName` varchar(255) NOT NULL,
  `price` varchar(10) NOT NULL,
  `productType` enum('DVD','Book','Furniture','') NOT NULL,
  `description` text NOT NULL,
  `regDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `productId`, `sku`, `productName`, `price`, `productType`, `description`, `regDate`) VALUES
(1, '609e2a5d8d66f', 'dh12344', 'terminator blu ray', '5.32', 'DVD', 'Size : 0.11 MB', '2021-05-14 06:44:29'),
(2, '609e2aa19e299', 'qso583948', 'sometimes in april', '2.10', 'DVD', 'Size : 1 MB', '2021-05-14 06:45:37'),
(3, '609e2acb9687f', 'ewr39204', 'zero dark thirty', '25', 'DVD', 'Size : 200 MB', '2021-05-14 06:46:19'),
(4, '609e2b07d1b63', 'tye2304324', 'tears of sun', '11.90', 'DVD', 'Size : 240 MB', '2021-05-14 06:47:19'),
(5, '609e2b40ce69f', 'nmvc2123034', 'what is true love', '230', 'DVD', 'Size : 700 MB', '2021-05-14 06:48:16'),
(6, '609e2b6c4bd5c', 'bkr20343', 'cashflow quadrant', '32.12', 'Book', 'Weight : 0.23 KG', '2021-05-14 06:49:00'),
(7, '609e2b9b6631b', 'kao2345', 'king james bible', '23.01', 'Book', 'Weight : 4 KG', '2021-05-14 06:49:47'),
(8, '609e2bd77892e', 'opw234503', 'tell magazine', '30', 'Book', 'Weight : 0.03 KG', '2021-05-14 06:50:47'),
(9, '609e2bfd5bc26', 'bkre094394', 'news time', '23.01', 'Book', 'Weight : 0.02 KG', '2021-05-14 06:51:25'),
(10, '609e2c652d969', 'fun239212', 'king-size bedframe', '400.35', 'Furniture', 'Dimensoin : 36 x 40 x 80', '2021-05-14 06:53:09'),
(11, '609e2caac9053', 'fun432201', 'detachable shoerack', '2.32', 'Furniture', 'Dimensoin : 45 x 28 x 18', '2021-05-14 06:54:18'),
(12, '609e2cdcdf78f', 'rc394020', 'imported recline chair', '120', 'Furniture', 'Dimensoin : 12 x 11 x 17', '2021-05-14 06:55:08'),
(13, '609e2d19e8989', 'din23012', 'dinning table set', '44.50', 'Furniture', 'Dimensoin : 22 x 12 x 25', '2021-05-14 06:56:09'),
(14, '609e2d4d8a473', 'mbr340291344', 'marble TV stand', '52.04', 'Furniture', 'Dimensoin : 10 x 13 x 19', '2021-05-14 06:57:01'),
(15, '609e2d892f188', 'qms29321392', 'quality modern sofa', '22.30', 'Furniture', 'Dimensoin : 23 x 34 x 21', '2021-05-14 06:58:01'),
(16, '609e2de0ea093', 'jum0392302', 'complete jumbo sofa', '62.87', 'Furniture', 'Dimensoin : 43 x 40 x 54', '2021-05-14 06:59:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
