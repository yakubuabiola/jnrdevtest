<?php
spl_autoload_register(function($className) {
	$file = __DIR__ . '\\' . $className . '.php';
	$file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
	if (file_exists($file)) {
		include $file;
	}
});

$path = __DIR__ . DIRECTORY_SEPARATOR;
$explode = explode('/', __DIR__);
switch ('http://'.$_SERVER['HTTP_HOST']) {
	case 'http://localhost':
		$base_url = 'http://'.$_SERVER['HTTP_HOST']. DIRECTORY_SEPARATOR.end($explode).DIRECTORY_SEPARATOR;
		break;	
	default:
		$base_url = 'http://'.$_SERVER['HTTP_HOST'].DIRECTORY_SEPARATOR;
		break;
}